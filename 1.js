const dbConnection = require('./connection')
module.exports = app => {
    const connection = dbConnection

    app.post('/register', (req, res) => {
        
        const {
            Cédula,
            apellidos,
            nombres, 
            dirección,
            teléfono
        } = req.body
        
        connection.query('INSERT INTO usuario SET ?', {
            Cédula,
            apellidos,
            nombres, 
            dirección,
            teléfono
        }, (err, result) => {
            res.redirect('/login')
        })
    })
}